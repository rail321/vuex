export default {
  state: () => ({
    users: [],
  }),

  getters: {
    getAllUsers: state => state.users,
  },

  actions: {
    generateUsers(context) {
      const users = [ ]

      for (let i = 1; i <= 5; i++) {
        users.push({
          name: `Ivan${ i }`,
          age: 10 * i,
          city: `City${ i }`
        })
      }

      context.commit('setUsers', users)
    },
    addUser(context, user) {
      context.commit('pushUser', user)
    },
    deleteUser(context, name) {
      context.commit('removeUser', name)
    }
  },

  mutations: {
    setUsers(state, users) {
      state.users = users
    },
    pushUser(state, user) {
      console.log(user)
      state.users.push(user)
    },
    removeUser(state, name) {
      state.users = state.users.filter(user => user.name !== name)
    }
  }
}