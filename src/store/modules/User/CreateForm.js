export default {
  state: () => ({
    name: null,
    age: null,
    city: null,
  }),

  getters: {
    getName: state => state.name,
    getAge: state => state.age,
    getCity: state => state.city,
  },

  actions: {
    onNameInput(context, input) {
      context.commit('setName', input)
    },
    onAgeInput(context, input) {
      context.commit('setAge', input)
    },
    onCityInput(context, input) {
      context.commit('setCity', input)
    },
    submit(context) {
      const user = {
        name: context.getters.getName,
        age: context.getters.getAge,
        city: context.getters.getCity,
      }

      context.dispatch('addUser', user)
    }
  },

  mutations: {
    setName(state, payload) {
      state.name = payload
    },
    setAge(state, payload) {
      state.age = payload
    },
    setCity(state, payload) {
      state.city = payload
    },
  }
}