import Vue from 'vue'
import Vuex from 'vuex'
import User from './modules/User'
import UserCreateForm from './modules/User/CreateForm'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    User, UserCreateForm
  }
})
